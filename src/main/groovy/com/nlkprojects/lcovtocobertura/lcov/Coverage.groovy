package com.nlkprojects.lcovtocobertura.lcov

import com.nlkprojects.lcovtocobertura.lcov.api.PackageCoverage
import com.nlkprojects.lcovtocobertura.lcov.api.metrics.MetricType
import com.nlkprojects.lcovtocobertura.lcov.api.utils.Tabbable
import com.nlkprojects.lcovtocobertura.lcov.api.xml.IXMLProvider

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

class Coverage implements Tabbable, IXMLProvider {

    private static final String XML_HEADER       = '''<?xml version="1.0" ?>'''
    private static final String COVERAGE_DOCTYPE = '''<!DOCTYPE coverage SYSTEM 'http://cobertura.sourceforge.net/xml/coverage-03.dtd'>'''

    String timestamp
    String lcovToolVersion

    private double linesFound
    private double linesHit
    private double methodsFound
    private double methodsHit
    private double branchesFound
    private double branchesHit

    private final List<String> sources = []
    private final Map<String, PackageCoverage> packages  = [:]

    private Coverage() {

    }

    Coverage(String lcovVersion, String timestamp) {
        this()
        this.lcovToolVersion = lcovVersion
        this.timestamp       = timestamp
    }

    void addSource(String src) {
        if (src != null) {
            sources.add(src)
        }
    }

    protected void addPackage(PackageCoverage pkg) {

        if (this.packages[pkg.identifier()] == null) {
            this.packages[pkg.identifier()] = pkg

            this.linesFound    += pkg.getCount(MetricType.MetricTypeLineCount)
            this.linesHit      += pkg.getCount(MetricType.MetricTypeLineHits)
            this.methodsFound  += pkg.getCount(MetricType.MetricTypeMethodCount)
            this.methodsHit    += pkg.getCount(MetricType.MetricTypeMethodHits)
            this.branchesFound += pkg.getCount(MetricType.MetricTypeBranchCount)
            this.branchesHit   += pkg.getCount(MetricType.MetricTypeBranchHits)
        }
    }

    @Override
    String toString() {
        provideXML()
    }

    @Override
    String provideXML() {
        provideXML(0)
    }

    @Override
    String provideXML(int t) {

        System.setProperty('javax.xml.accessExternalSchema', 'all')
        StringBuilder sb = new StringBuilder()

        sb.append(tab(t)).append(XML_HEADER).append("\n")
        sb.append(tab(t)).append(COVERAGE_DOCTYPE).append("\n")

        double method_rate = 0.0
        double line_rate   = 0.0
        double branch_rate = 0.0

        sb.append(tab(t)).append("<coverage method-rate=\"${method_rate}\" line-rate=\"${line_rate}\" ")
        sb.append("branch-rate=\"${branch_rate}\" timestamp=\"${this.timestamp}\" version=\"${this.lcovToolVersion}\">").append("\n")

        //Sources
        sb.append(tab(t+1)).append("<sources>").append("\n")

        for (String s : this.sources) {
            sb.append(tab(t+2)).append("<source>${s}</source>").append("\n")
        }

        sb.append(tab(t+1)).append("</sources>").append("\n")

        //Package
        sb.append(tab(t+1)).append("<packages>").append("\n")

        List<PackageCoverage> sortedPackages = this.packages.values().sort(false, new Comparator<PackageCoverage>() {
            @Override
            int compare(PackageCoverage o1, PackageCoverage o2) {
                o1.identifier() <=> o2.identifier()
            }
        })

        for (PackageCoverage pkg : sortedPackages) {
            sb.append(pkg.provideXML(t+2)).append("\n")
        }

        sb.append(tab(t+1)).append("</packages>").append("\n")
        sb.append(tab(t)).append("</coverage>")
        sb.toString()
    }
}
