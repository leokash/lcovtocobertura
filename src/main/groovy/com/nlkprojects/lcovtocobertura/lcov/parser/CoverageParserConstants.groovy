package com.nlkprojects.lcovtocobertura.lcov.parser

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */
@SuppressWarnings('unused')
final class CoverageParserConstants {
    //Package Information regular expressions

    static final TN_REGEX            = ~/^TN:(.*)?/
    static final SRC_REGEX           = ~/^[SK]F:(.*)?/
    static final END_OF_RECORD_REGEX = ~/^end_of_record(.*)?/ //Package delimiter

    //Package - Line regex
    static final LINES_FOUND_REGEX = ~/^LF:(.*)?/
    static final LINES_HITS_REGEX  = ~/^LH:(.*)?/
    static final LINE_INFO_REGEX   = ~/^DA:(.*)?/

    //Package - Function regex
    static final FUNCTIONS_REGEX       = ~/^FN:(.*)?/
    static final FUNCTIONS_INFO_REGEX  = ~/^FNDA:(.*)?/
    static final FUNCTIONS_FOUND_REGEX = ~/^FNF:(.*)?/
    static final FUNCTIONS_HITS_REGEX  = ~/^FNH:(.*)?/

    //Package - Branches regex
    static final BRANCHES_INFO_REGEX  = ~/^BRDA:(.*)?/
    static final BRANCHES_FOUND_REGEX = ~/^BRF:(.*)?/
    static final BRANCHES_HITS_REGEX  = ~/^BRH:(.*)?/
}
