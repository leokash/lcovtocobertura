package com.nlkprojects.lcovtocobertura.lcov.parser

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

trait LCovParser<R> {
    abstract R parseReport(List<String> report)
}