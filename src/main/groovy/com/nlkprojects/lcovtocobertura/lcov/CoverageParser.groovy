package com.nlkprojects.lcovtocobertura.lcov

import com.nlkprojects.lcovtocobertura.lcov.api.BranchCoverage
import com.nlkprojects.lcovtocobertura.lcov.api.ClassCoverage
import com.nlkprojects.lcovtocobertura.lcov.api.LineCoverage
import com.nlkprojects.lcovtocobertura.lcov.api.MethodCoverage
import com.nlkprojects.lcovtocobertura.lcov.api.PackageCoverage
import com.nlkprojects.lcovtocobertura.lcov.api.collections.list.MultiList
import com.nlkprojects.lcovtocobertura.lcov.api.metrics.MetricType
import com.nlkprojects.lcovtocobertura.lcov.parser.LCovParser

import com.nlkprojects.lcovtocobertura.lcov.parser.CoverageParserConstants

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

@SuppressWarnings('unused')
class CoverageParser implements LCovParser<Coverage>{

    private CoverageParser() {

    }

    private String projectDirectory
    private CoverageParserOptions options

    CoverageParser(String baseDir, CoverageParserOptions options) {
        this()
        this.projectDirectory = baseDir
        this.options          = options
    }

    Coverage parse(File input) {
        if (input != null && input.isFile() && input.canRead()) {
            return parseReport(input.readLines())
        }

        return null
    }

    @Override
    Coverage parseReport(List<String> report) {
        Coverage coverage = new Coverage(this.options.getLCovVersion(), this.options.getTimestamp())
        coverage.addSource(this.projectDirectory)

        Map<String, Map<String , ?>> processedData = processReportFile(report)
        parseReportPackage(coverage, this.projectDirectory, processedData)
        coverage
    }

    private static List<String> cleanup(List<String> list) {
        List<String> retVal = []
        list.each {l ->
            String newLine = l.trim()
            if (newLine != null && newLine.length() > 0) {
                retVal.add(newLine)
            }
        }

        retVal.last().matches(CoverageParserConstants.END_OF_RECORD_REGEX) ? retVal : []
    }

    private static Map<String, Map<String, ?>> processReportFile(List<String> reports) {

        Map<String, Map<String, ?>> processedInfo = [:]
        reports = cleanup(reports)
        if (reports.isEmpty()) {
            return processedInfo
        }

        processClassInfo(0, reports, processedInfo)
        processedInfo
    }

    private static List<String> splitCoverageLine(String line) {
        List<String> retVal = []
        int index = line.indexOf(":")
        if (index != -1) {
            retVal[0] = line.substring(0, index+1)
            if ((index + 1) <= line.length()) {
                retVal[1] = line.substring(index + 1, line.length())
            }
        }
        retVal
    }

    private static void processClassInfo(int index, List<String> reports, Map<String, Map<String, ?>> classInfoMap) {
        int totalReports = reports.size()
        if (index >= 0 && index < totalReports) {
            Map<String, ?> clazzMap = [:]

            String clazz = ""
            for (int i = index; i < totalReports; i++) {
                String s = reports[i]
                if (s.matches(CoverageParserConstants.END_OF_RECORD_REGEX)) {
                    index = i + 1
                    break
                }else if (s.matches(CoverageParserConstants.SRC_REGEX)) {
                    clazz = splitCoverageLine(s)[1]
                    continue
                }

                processCoverageLine(s, clazzMap)
            }

            if (clazz.length() > 0) {
                classInfoMap[clazz] = clazzMap
            }
            processClassInfo(index, reports, classInfoMap)
        }
    }

    private static void processCoverageLine(String line, Map<String, ?> clazzInfo) {
        if (line != null && line.length() > 0) {
            def arr = splitCoverageLine(line)

            if (arr.size() == 2) {
                switch (arr[0]) {
                    case CoverageParserConstants.LINE_INFO_REGEX:
                    case CoverageParserConstants.FUNCTIONS_REGEX:
                    case CoverageParserConstants.FUNCTIONS_INFO_REGEX:
                    case CoverageParserConstants.BRANCHES_INFO_REGEX:
                        handleMultiListLine(arr[0], arr[1], clazzInfo)
                        break
                    default:
                        clazzInfo[arr[0]] = arr[1]
                }
            }
        }
    }

    private static void handleMultiListLine(String key, String line, Map<String, ?> classInfo) {
        MultiList<String, String> ml = (MultiList<String, String>)classInfo[key]
        if (ml == null) {
            ml = new MultiList<>()
            classInfo[key] = ml
        }

        ml.add(key, line)
    }

    private static String packageName(String baseDir, List<String> pkgArr) {

        Closure<String> c = {
            baseDir.split(File.separator).last()
        }

        if (pkgArr == null || pkgArr.size() == 0) {
            return c()
        } else if (pkgArr.size() == 1) {
            if (pkgArr[0].length() > 0) {
                return pkgArr[0]
            }

            return c()
        }

        pkgArr.join(".")
    }
    private static void parseReportPackage(Coverage coverage, String baseDir, Map<String, Map<String, ?>> classes) {
        Map<String, PackageCoverage> packages = [:]

        classes.entrySet().each { Map.Entry<String, Map<String, ?>> e ->
            List<String> pkgArr = e.getKey().replace(baseDir, "").trim().split(File.separator)
            String clazzName    = pkgArr.last()
            pkgArr.remove(clazzName)
            String pkgName = packageName(baseDir, pkgArr)

            PackageCoverage pkgCover = packages[pkgName]
            if (pkgCover == null) {
                pkgCover = new PackageCoverage(pkgName)
                packages[pkgName] = pkgCover
            }

            parseClassReport(clazzName, pkgCover, e.getValue())
        }

        packages.values().each {p ->
            coverage.addPackage(p)
        }
    }

    static private void parseClassReport(String name, PackageCoverage coverage, Map<String, ?> classInfo) {

        int lf = 0
        int lh = 0
        int mf = 0
        int mh = 0
        int bf = 0
        int bh = 0

        List<String> branchesArr  = []
        List<String> linesArr     = []
        List<String> functionsArr = []
        List<String> funcInfoArr  = []

        classInfo.entrySet().each {Map.Entry<String, ?> e ->
            switch (e.getKey()) {
                case CoverageParserConstants.LINES_FOUND_REGEX:
                    lf = ((String)e.getValue()).toInteger()
                    break

                case CoverageParserConstants.LINES_HITS_REGEX:
                    lh = ((String)e.getValue()).toInteger()
                    break

                case CoverageParserConstants.FUNCTIONS_FOUND_REGEX:
                    mf = ((String)e.getValue()).toInteger()
                    break

                case CoverageParserConstants.FUNCTIONS_HITS_REGEX:
                    mh = ((String)e.getValue()).toInteger()
                    break

                case CoverageParserConstants.BRANCHES_FOUND_REGEX:
                    bf = ((String)e.getValue()).toInteger()
                    break

                case CoverageParserConstants.BRANCHES_HITS_REGEX:
                    bh = ((String)e.getValue()).toInteger()
                    break

                case CoverageParserConstants.BRANCHES_INFO_REGEX:
                    branchesArr = ((MultiList<String, String>)e.getValue()).allObjects()
                    break

                case CoverageParserConstants.LINE_INFO_REGEX:
                    linesArr = ((MultiList<String, String>)e.getValue()).allObjects()
                    break

                case CoverageParserConstants.FUNCTIONS_REGEX:
                    functionsArr = ((MultiList<String, String>)e.getValue()).allObjects()
                    break

                case CoverageParserConstants.FUNCTIONS_INFO_REGEX:
                    funcInfoArr = ((MultiList<String, String>)e.getValue()).allObjects()
                    break

                default:
                    break
            }
        }

        String filename  = coverage.getName().replaceAll(/\./,File.separator) + "${File.separator}" + name
        String clazzName = name.replaceAll(/\./,"_")
        ClassCoverage clazzCover = new ClassCoverage(clazzName, filename, lf, lh, mf, mh, bf, bh)

        Map<Integer, BranchCoverage> branches = parseBranchesReport(branchesArr)

        Map<Integer, LineCoverage> lines = parseLineReport(linesArr, branches)

        parseMethodReport(functionsArr, funcInfoArr, lines).each { m ->
            clazzCover.addMethod(m)
        }

        coverage.addClass(clazzCover)
    }

    static private Map<Integer, BranchCoverage> parseBranchesReport(List<String> list) {
        //Branch info computing
        Map<Integer, BranchCoverage> branches = [:]

        if (list == null) {
            return branches
        }

        list.each { s ->
            def arr  = s.split(",")
            int id   = arr[0].toInteger()
            int hits = (arr[3].equalsIgnoreCase("-") ? 0 : arr[3].toInteger())
            BranchCoverage b = BranchCoverage.createBranch(id, arr[1].toInteger(), arr[2].toInteger(), hits, {int i ->
                branches[i]
            })
            branches[id] = b
        }

        branches
    }

    static private Map<Integer, LineCoverage> parseLineReport(List<String> list, Map<Integer, BranchCoverage> branches) {
        //Line info computing
        Map<Integer, LineCoverage> lines = [:]

        if (list == null) {
            return lines
        }

        list.each { s ->
            def arr   = s.split(",")
            int id    = arr[0].toInteger()
            int hits  = arr[1].toInteger()
            lines[id] = new LineCoverage(id, hits, branches[id])
        }
        lines
    }

    static private List<MethodCoverage> parseMethodReport(List<String> functionsList, List<String> funcInfoList, Map<Integer, LineCoverage> lines) {

        //Method info computing
        Map<String, MethodCoverage> methods = [:]

        if (functionsList == null || functionsList.isEmpty()) {
            return methods.values().asList()
        }

        functionsList.each { s ->
            def info = s.split(",")
            MethodCoverage m = new MethodCoverage(info[1], info[0].toInteger())
            methods[m.identifier()] = m
        }


        if (funcInfoList != null && !funcInfoList.isEmpty()) {
            funcInfoList.each { s ->
                def info = s.split(",")
                MethodCoverage m = methods[info[1]]
                if (m != null) {
                    m.increment(MetricType.MetricTypeMethodHits, info[0].toInteger())
                }
            }
        }

        List<MethodCoverage> sortedMethods = methods.values().sort(false, new Comparator<MethodCoverage>() {
            @Override
            int compare(MethodCoverage o1, MethodCoverage o2) {
                if (o1.startLine() < o2.startLine()) {
                    return -1
                } else if (o1.startLine() > o2.startLine()) {
                    return 1
                }
                0
            }
        })

        for (int i = 0; i < sortedMethods.size(); i++) {
            if ((i + 1) < sortedMethods.size()) {
                sortedMethods[i].computeLimiters(sortedMethods[i+1])
            }
        }

        methods.values().each { m ->
            lines.each {k, v ->
                if (m.containsLine(k)) {
                    m.addLine(v)
                }
            }
        }

        methods.values().asList()
    }
}
