package com.nlkprojects.lcovtocobertura.lcov.api

import com.nlkprojects.lcovtocobertura.lcov.api.collections.map.CoverageMap
import com.nlkprojects.lcovtocobertura.lcov.api.utils.Identifiable
import com.nlkprojects.lcovtocobertura.lcov.api.metrics.Metric
import com.nlkprojects.lcovtocobertura.lcov.api.metrics.MetricType
import com.nlkprojects.lcovtocobertura.lcov.api.utils.Tabbable
import com.nlkprojects.lcovtocobertura.lcov.api.xml.IXMLProvider

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

@SuppressWarnings('unused')
class ClassCoverage implements Metric, Identifiable<String> , Tabbable, IXMLProvider {

    Integer complexity = 0

    String filename
    String name

    private final CoverageMap<String, MethodCoverage> methods = new CoverageMap<>(null)

    private ClassCoverage() {

    }

    ClassCoverage(String name, String filename, int lf, int lh, int mf, int mh, int bf, int bh) {
        this()
        this.name     = name
        this.filename = filename

        increment(MetricType.MetricTypeLineHits, lh)
        increment(MetricType.MetricTypeLineCount, lf)
        increment(MetricType.MetricTypeMethodCount, mf)
        increment(MetricType.MetricTypeMethodHits, mh)
        increment(MetricType.MetricTypeBranchCount, bf)
        increment(MetricType.MetricTypeBranchHits, bh)
    }

    @Override
    String identifier() {
        return this.name
    }

    void addMethod(MethodCoverage method) {
        methods.add(method.identifier(), method, [].asList())
    }

    @Override
    String toString() {
        provideXML()
    }

    @Override
    String provideXML() {
        provideXML(0)
    }

    @Override
    String provideXML(int t) {
        StringBuilder sb = new StringBuilder()

        double mf = this.getCount(MetricType.MetricTypeMethodCount)
        double mh = this.getCount(MetricType.MetricTypeMethodHits)
        double lf = this.getCount(MetricType.MetricTypeLineCount)
        double lh = this.getCount(MetricType.MetricTypeLineHits)
        double bf = this.getCount(MetricType.MetricTypeBranchCount)
        double bh = this.getCount(MetricType.MetricTypeBranchHits)

        double method_rate = (mf > 0 && mh > 0) ? (mh / mf) : 0.0
        double line_rate   = (lf > 0 && lf > 0) ? (lh / lf) : 0.0
        double branch_rate = (bf > 0 && bh > 0) ? (bh / bf) : 0.0

        sb.append(tab(t))
        sb.append("<class method-rate=\"${method_rate}\" line-rate=\"${line_rate}\" branch-rate=\"${branch_rate}\" complexity=\"${this.complexity}\" name=\"${this.name}\" filename=\"${this.filename}\">")
        sb.append("\n")

        sb.append(tab(t+1)).append("<methods>").append("\n")
        List<MethodCoverage> sortedMethods = this.methods.values().sort(false, new Comparator<MethodCoverage>() {
            @Override
            int compare(MethodCoverage m1, MethodCoverage m2) {
                m1.startLine() <=> m2.startLine()
            }
        })

        for (MethodCoverage m : sortedMethods) {
            sb.append(m.provideXML(t+2)).append("\n")
        }

        sb.append(tab(t+1)).append("</methods>").append("\n")
        sb.append(tab(t)).append("</class>")
        sb.toString()
    }
}
