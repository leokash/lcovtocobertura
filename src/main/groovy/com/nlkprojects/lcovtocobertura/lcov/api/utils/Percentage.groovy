package com.nlkprojects.lcovtocobertura.lcov.api.utils

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

trait Percentage {
    /**
     * Calculates the percentage of (v/t)*100
     *
     * @param v         Elements
     * @param t         Total amount of element
     * @param decimal   Decimals to display
     * @return          String representation of the percentage
     */
    String percent(int v, int t, int decimal) {
        if (v == 0) {
            return "0"
        }else if (v != t) {
            String percentString = "${((double)v / (double)t) * 100}"
            def percent = percentString.split(/\./)
            if (decimal > 0) {
                if (new BigDecimal(percent[1]) == 0.0) {
                    return percent[0]
                }

                String retVal   = percent[0]
                char[] decimals = percent[1].toCharArray()

                if (decimals.length > 0) {
                    retVal+= "."
                }
                for (i in 0..(decimal - 1)) {
                    if (i < decimals.length) {
                        retVal += decimals[i]
                    }
                }

                return retVal
            }

            return percent[0]
        }

        "100"
    }
}