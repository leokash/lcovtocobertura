package com.nlkprojects.lcovtocobertura.lcov.api.metrics.container

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

interface Container<T>{
    T getContent()
}
