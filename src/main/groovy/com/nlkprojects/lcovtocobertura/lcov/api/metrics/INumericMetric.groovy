package com.nlkprojects.lcovtocobertura.lcov.api.metrics

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

interface INumericMetric {
    void increment(MetricType type)
    void increment(MetricType type, int count)
}