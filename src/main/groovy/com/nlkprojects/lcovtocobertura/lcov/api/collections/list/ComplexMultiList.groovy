package com.nlkprojects.lcovtocobertura.lcov.api.collections.list

import com.nlkprojects.lcovtocobertura.lcov.api.utils.Identifiable

/**
 *  Copyright (c) 2017 Kash Kabeya
 *  This is free software, licensed under the Apache License, Version 2.0,
 *  available in the accompanying LICENSE file.
 */
class ComplexMultiList<K, V extends Identifiable<K>> extends AbstractMultiList <K, V> {

    boolean add(V value) {
        if (value == null) {
            return false
        }

        K key = value.identifier()
        super.add(key, value)
    }

    boolean removeElement(V value) {
        if (value == null) {
            return false
        }

        List<V> l = this.map[value.identifier()]
        if (l == null) {
            return false
        }

        boolean retVal = l.remove(value)
        if (l.isEmpty()) {
            cleanUp([value.identifier()].asList())
        }

        retVal
    }
}
