package com.nlkprojects.lcovtocobertura.lcov.api

import com.nlkprojects.lcovtocobertura.lcov.api.utils.Identifiable
import com.nlkprojects.lcovtocobertura.lcov.api.utils.Percentage
import com.nlkprojects.lcovtocobertura.lcov.api.utils.Tabbable
import com.nlkprojects.lcovtocobertura.lcov.api.xml.IXMLProvider

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

@SuppressWarnings('unused')
class LineCoverage implements Identifiable<Integer>, Tabbable, IXMLProvider, Percentage {

    int number
    int hitCount
    private BranchCoverage branchCoverage

    LineCoverage() {

    }

    LineCoverage(int num, int hit, BranchCoverage branch) {
        this()
        this.number         = num
        this.hitCount       = hit
        this.branchCoverage = branch
    }

    int branchesCount() {
        if (this.branchCoverage != null) {
            return this.branchCoverage.pathsCount()
        }

        0
    }

    int branchesHit() {
        if (this.branchCoverage != null) {
            return this.branchCoverage.pathsHit()
        }

        return 0
    }

    @Override
    Integer identifier() {
        return this.number
    }

    @Override
    String provideXML() {
        provideXML(0)
    }

    @Override
    String provideXML(int t) {
        StringBuilder sb = new StringBuilder()

        sb.append(tab(t))
        if (this.branchCoverage != null) {
            String coverage = "${percent(this.branchesHit(), this.branchesCount(), 0)}% (${this.branchCoverage.pathsHit()}/${this.branchCoverage.pathsCount()})"
            sb.append("<line branch=\"true\" condition-coverage=\"${coverage}\" hits=\"${this.hitCount}\" number=\"${this.number}\">").append("\n")
            sb.append(this.branchCoverage.provideXML(t+1)).append("\n")
            sb.append(tab(t)).append("</line>")
        } else {
            sb.append("<line branch=\"false\" hits=\"${this.hitCount}\" number=\"${this.number}\"/>")
        }


        sb.toString()
    }

    @Override
    String toString() {
        provideXML()
    }
}
