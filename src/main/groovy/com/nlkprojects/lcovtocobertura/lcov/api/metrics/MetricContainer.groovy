package com.nlkprojects.lcovtocobertura.lcov.api.metrics

import com.nlkprojects.lcovtocobertura.lcov.api.metrics.container.AbstractContainer
import com.nlkprojects.lcovtocobertura.lcov.api.metrics.container.NumericContainer

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

@SuppressWarnings('unused')
class MetricContainer<Integer> extends AbstractContainer<Integer> implements NumericContainer {

    protected MetricContainer(int value) {
        this.content = value
    }

    void incrementValue() {
        this.content += 1
    }

    void incrementValue(int count) {
        this.content += count
    }
}
