package com.nlkprojects.lcovtocobertura.lcov.api.metrics
/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

trait Metric implements IMetric , INumericMetric {

    private Map<MetricType, MetricContainer<Integer>> metrics = new HashMap<>()

    private MetricContainer<Integer> getContainer (MetricType type) {

        MetricContainer container = metrics.get(type)

        if (!container) {
            container = new MetricContainer(0)
            metrics.put(type, container)
        }

        container
    }

    @Override
    void increment(MetricType type) {
        getContainer(type).incrementValue()
    }

    @Override
    void increment(MetricType type, int count) {
        getContainer(type).incrementValue(count)
    }

    @Override
    int getCount(MetricType type) {
        MetricContainer container = metrics.get(type)

        if (!container) {
            return 0
        }

        container.getContent()
    }
}
