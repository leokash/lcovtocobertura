package com.nlkprojects.lcovtocobertura.lcov.api

import com.nlkprojects.lcovtocobertura.lcov.api.utils.Percentage
import com.nlkprojects.lcovtocobertura.lcov.api.utils.Tabbable
import com.nlkprojects.lcovtocobertura.lcov.api.xml.IXMLProvider

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

@SuppressWarnings('unused')
class BranchCoverage implements IXMLProvider, Tabbable, Percentage {

    int identifier

    private int pathsHit

    private final List<BranchCoverageElement> paths = []

    private BranchCoverage() {

    }

    private BranchCoverage(int id) {
        this()
        this.identifier = id
    }

    int pathsHit() {
        this.pathsHit
    }

    int pathsCount() {
        paths.size()
    }

    static BranchCoverage createBranch(int id, int block, int path, int hits, Closure<BranchCoverage> closure) {
        BranchCoverage branch = closure(id)
        branch = branch != null ? branch : new BranchCoverage(id)

        BranchCoverageElement pathElement = new BranchCoverageElement(block, path, hits)
        branch.paths.add(pathElement)
        branch.pathsHit += (hits > 0 ? 1 : 0)
        branch
    }

    protected static class BranchCoverageElement {

        int block
        int path
        int hits

        protected BranchCoverageElement(int block, int path, int hits) {
            this.block = block
            this.path  = path
            this.hits  = hits
        }
    }

    @Override
    String provideXML() {
        provideXML(0)
    }

    @Override
    String provideXML(int t) {
        StringBuilder sb = new StringBuilder()

        List<BranchCoverageElement> sortedPaths = paths.sort(false, new Comparator<BranchCoverageElement>() {
            @Override
            int compare(BranchCoverageElement o1, BranchCoverageElement o2) {
                o1.path <=> o2.path
            }
        })

        sb.append(tab(t))
        sb.append("<conditions>\n")

        sortedPaths.each { p ->
            sb.append(tab(t+1))
            sb.append("<condition coverage=\"${percent((p.hits > 0 ? 1 : 0), 1, 0)}%\" number=\"${p.path}\" type=\"jump\"/>")
            sb.append("\n")
        }

        sb.append(tab(t))
        sb.append("</conditions>")
        sb.toString()
    }

    @Override
    String toString() {
        provideXML()
    }
}
