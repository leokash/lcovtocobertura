package com.nlkprojects.lcovtocobertura.lcov.api.collections.list

import java.util.Map.Entry

/**
 *  Copyright(c) 2017 Kash Kabeya
 *  This is free software, licensed under the Apache License, Version 2.0,
 *  available in the accompanying LICENSE file.
 */
class AbstractMultiList<K, V> implements IMultiList<K, V> {
    protected final Map<K, List<V>> map = [:]

    boolean add(K key, V value) {
        if (key == null || value == null) {
            return false
        }

        List<V> l = this.map[key]
        if (l == null) {
            l = []
            this.map.put(key, l)
        }
        l.add(value)
    }

    boolean remove(K key) {
        if (key == null) {
            return false
        }

        this.map.remove(key) != null
    }

    boolean removeElement(V value) {
        if (value == null) {
            return false
        }
        boolean retVal = false
        List<K> emptyLists = []

        for (Entry<K, List<K>> e : this.map.entrySet()) {
            if (e.value.remove((Object)value)) {
                retVal = true
                if (e.value.isEmpty()) {
                    emptyLists.add(e.key)
                }
            }
        }

        cleanUp(emptyLists)
        retVal
    }

    protected void cleanUp(List<K> keys) {
        for (K key : keys) {
            this.map.remove(key)
        }
    }

    List<V> allObjects() {
        allObjects(null)
    }

    List<V> allObjects(Closure<Boolean> c) {
        List<V> retVal = []
        for (List<V> l : this.map.values()) {
            if (c != null) {
                l.each { v ->
                    if (c(v)) {
                        retVal.add(v)
                    }
                }
            } else {
                retVal.addAll(l)
            }
        }
        retVal
    }

    void clear() {
        this.map.clear()
    }

    int size() {
        this.allObjects().size()
    }
}
