package com.nlkprojects.lcovtocobertura.lcov.api.metrics

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

enum MetricType {
    //Line Metrics
    MetricTypeLineHits,
    MetricTypeLineCount,

    //Method Metrics
    MetricTypeMethodHits,
    MetricTypeMethodCount,

    //Branch Metrics
    MetricTypeBranchHits,
    MetricTypeBranchCount
}