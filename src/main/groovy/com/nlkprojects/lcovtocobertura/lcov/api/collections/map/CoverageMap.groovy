package com.nlkprojects.lcovtocobertura.lcov.api.collections.map

import com.nlkprojects.lcovtocobertura.lcov.api.metrics.Metric
import com.nlkprojects.lcovtocobertura.lcov.api.metrics.MetricType

/**
 *  Copyright (c) 2017 Kash Kabeya
 *  This is free software, licensed under the Apache License, Version 2.0,
 *  available in the accompanying LICENSE file.
 */

@SuppressWarnings('unused')
class CoverageMap<K, V> implements Metric {

    @Delegate private final Map<K, V> coverageMap = [:]

    @SuppressWarnings('GrFinalVariableAccess')
    private final Closure<Integer> closure

    private CoverageMap() {

    }

    CoverageMap (Closure<Integer> closure) {
        this()
        this.closure = closure
    }

    boolean add(K key, V value, List<MetricType> types) {

        boolean present = this.coverageMap.get(key) != null

        Object retVal
        if (!present) {
            retVal = this.coverageMap.put(key, value)
        }

        if (this.closure) {
            types.each { t ->
                increment(t, this.closure(present, t, value))
            }
        }

        !present && retVal == null
    }

}
