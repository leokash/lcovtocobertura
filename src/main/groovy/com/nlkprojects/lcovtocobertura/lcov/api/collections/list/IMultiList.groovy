package com.nlkprojects.lcovtocobertura.lcov.api.collections.list

/**
 *  Copyright (c) 2017 Kash Kabeya
 *  This is free software, licensed under the Apache License, Version 2.0,
 *  available in the accompanying LICENSE file.
 */
interface IMultiList<K, V> {
    boolean add(K key, V value)
    boolean remove(K key)
    boolean removeElement(V value)
    List<V> allObjects()
    List<V> allObjects(Closure<Boolean> closure)
    void clear()
    int size()
}
