package com.nlkprojects.lcovtocobertura.lcov.api

import com.nlkprojects.lcovtocobertura.lcov.api.collections.map.CoverageMap
import com.nlkprojects.lcovtocobertura.lcov.api.utils.Identifiable
import com.nlkprojects.lcovtocobertura.lcov.api.metrics.Metric
import com.nlkprojects.lcovtocobertura.lcov.api.metrics.MetricType
import com.nlkprojects.lcovtocobertura.lcov.api.utils.Percentage
import com.nlkprojects.lcovtocobertura.lcov.api.utils.Tabbable
import com.nlkprojects.lcovtocobertura.lcov.api.xml.IXMLProvider

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

@SuppressWarnings('unused')
class MethodCoverage implements Metric, Identifiable<String>, Tabbable, IXMLProvider {

    private CoverageMap<Integer, LineCoverage> lines

    private String name
    private String signature

    private Integer methodStartDelimiter
    private Integer methodEndDelimiter

    private MethodCoverage() {

    }

    private static Closure<Integer> metricCallback() {
        {boolean p, MetricType t, LineCoverage l ->
            if (p) {
                return 0
            }

            switch (t) {
                case MetricType.MetricTypeLineHits:
                    return l.hitCount > 0 ? 1 : 0

                case MetricType.MetricTypeLineCount:
                    return  1

                case MetricType.MetricTypeBranchHits:
                    return l.branchesHit()

                case MetricType.MetricTypeBranchCount:
                    return l.branchesCount()

                default:
                    0
            }
        }
    }

    MethodCoverage(String name, int start) {
        this(name, null, start)
    }

    MethodCoverage(String name, String signature, int start) {
        this()
        this.name                 = name
        this.signature            = signature
        this.methodStartDelimiter = start
        this.lines                = new CoverageMap<>(metricCallback())
    }

    int startLine() {
        this.methodStartDelimiter
    }

    @Override
    String identifier() {
        return this.name
    }

    void computeLimiters(MethodCoverage otherMethod) {
        if (otherMethod != null && this != otherMethod) {
            this.methodEndDelimiter = otherMethod.methodStartDelimiter
        }
    }

    boolean containsLine(int line) {
        if (this.methodEndDelimiter != null) {
            return this.methodStartDelimiter <= line && this.methodEndDelimiter >= line
        }

        this.methodStartDelimiter <= line
    }

    void addLine(LineCoverage line) {
        def metrics = [MetricType.MetricTypeLineHits, MetricType.MetricTypeLineCount,
                       MetricType.MetricTypeBranchHits, MetricType.MetricTypeBranchCount]
        this.lines.add(line.number, line, metrics.asList())
    }

    @Override
    int getCount(MetricType type) {
        this.lines.getCount(type)
    }

    @Override
    String provideXML() {
        provideXML(0)
    }

    @Override
    String provideXML(int t) {
        double lf = this.getCount(MetricType.MetricTypeLineCount)
        double lh = this.getCount(MetricType.MetricTypeLineHits)
        double bf = this.getCount(MetricType.MetricTypeBranchCount)
        double bh = this.getCount(MetricType.MetricTypeBranchHits)

        StringBuilder sb = new StringBuilder()
        sb.append(tab(t))
        sb.append("<method name=\"${this.name}\" ")
        if (this.signature != null) {
            sb.append("signature=\"${this.signature }\" ")
        }

        double line_rate   = lf > 0 && lh > 0 ? (lh / lf) : 0.0f
        double branch_rate = bf > 0 && bh > 0 ? (bh / bf) : 0.0f
        sb.append("line-rate=\"${line_rate}\" branch-rate=\"${branch_rate}\">").append("\n")

        sb.append(tab(t+1)).append("<lines>").append("\n")
        List<LineCoverage> sortedLines = this.lines.values().sort(false, new Comparator<LineCoverage>() {
            @Override
            int compare(LineCoverage l1, LineCoverage l2) {
                l1.number <=> l2.number
            }
        })
        for (LineCoverage l in sortedLines) {
            sb.append(l.provideXML(t+2)).append("\n")
        }

        sb.append(tab(t+1)).append("</lines>").append("\n")
        sb.append(tab(t)).append("</method>")
        sb.toString()
    }

    @Override
    String toString() {
        provideXML()
    }
}
