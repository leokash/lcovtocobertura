package com.nlkprojects.lcovtocobertura.lcov.api.utils

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

trait Tabbable {
    String tab(int tab) {
        if (tab > 0) {
            String t = ""
            for (int i = 0; i < tab; i++) {
                t += "    "
            }

            return t
        }

        ""
    }
}