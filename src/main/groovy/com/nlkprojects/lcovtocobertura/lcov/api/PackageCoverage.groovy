package com.nlkprojects.lcovtocobertura.lcov.api

import com.nlkprojects.lcovtocobertura.lcov.api.collections.map.CoverageMap
import com.nlkprojects.lcovtocobertura.lcov.api.metrics.Metric

import com.nlkprojects.lcovtocobertura.lcov.api.metrics.MetricType
import com.nlkprojects.lcovtocobertura.lcov.api.utils.Identifiable
import com.nlkprojects.lcovtocobertura.lcov.api.utils.Tabbable
import com.nlkprojects.lcovtocobertura.lcov.api.xml.IXMLProvider


/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE.txt file.
 */

@SuppressWarnings('unused')
class PackageCoverage implements Metric , IXMLProvider, Tabbable, Identifiable<String> {

    int complexity = 0
    private String name
    private CoverageMap<String, ClassCoverage> classes

    private PackageCoverage() {

    }

    PackageCoverage(String n) {
        this()
        this.name    = n
        this.classes = new CoverageMap<>({boolean added, MetricType type, ClassCoverage clazz ->
            added ? 0 : clazz.getCount(type)
        })
    }

    String getName() {
        new String(this.name)
    }

    @Override
    String identifier() {
        this.name
    }

    void addClass(ClassCoverage clazz) {
        def metrics = [MetricType.MetricTypeLineHits, MetricType.MetricTypeLineCount, MetricType.MetricTypeMethodHits,
                       MetricType.MetricTypeMethodCount, MetricType.MetricTypeBranchHits, MetricType.MetricTypeBranchCount]
        this.classes.add(clazz.identifier(),clazz, metrics.asList())
    }

    @Override
    int getCount(MetricType type) {
        int count = this.classes.getCount(type)
        if (count > 0) {
            return count
        }

        //Attempt retrieval from base metric class
        Metric.super.getCount(type)
    }

    @Override
    String toString() {
        provideXML()
    }

    @Override
    String provideXML() {
        provideXML(0)
    }

    @Override
    String provideXML(int t) {
        StringBuilder sb = new StringBuilder()

        double mf = this.getCount(MetricType.MetricTypeMethodCount)
        double mh = this.getCount(MetricType.MetricTypeMethodHits)
        double lf = this.getCount(MetricType.MetricTypeLineCount)
        double lh = this.getCount(MetricType.MetricTypeLineHits)
        double bf = this.getCount(MetricType.MetricTypeBranchCount)
        double bh = this.getCount(MetricType.MetricTypeBranchHits)

        double method_rate = (mf > 0 && mh > 0) ? (mh / mf) : 0.0f
        double line_rate   = (lf > 0 && lf > 0) ? (lh / lf) : 0.0f
        double branch_rate = (bf > 0 && bh > 0) ? (bh / bf) : 0.0f

        sb.append(tab(t))
        sb.append("<package method-rate=\"${method_rate}\" line-rate=\"${line_rate}\" branch-rate=\'${branch_rate}\' ")
        sb.append("complexity=\"${this.complexity}\" name=\"${this.name}\">")
        sb.append("\n")

        sb.append(tab(t+1)).append("<classes>").append("\n")

        //Classes
        List<ClassCoverage> sortedClasses = this.classes.values().sort(false, new Comparator<ClassCoverage>() {
            @Override
            int compare(ClassCoverage c1, ClassCoverage c2) {
                c1.getName() <=> c2.getName()
            }
        })
        for (ClassCoverage clazz : sortedClasses) {
            sb.append(clazz.provideXML(t+2)).append("\n")
        }

        sb.append(tab(t+1)).append("</classes>").append("\n")
        sb.append(tab(t)).append("</package>")

        sb.toString()
    }
}
