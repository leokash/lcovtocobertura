package com.nlkprojects.lcovtocobertura.lcov

import java.util.regex.Pattern

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

class CoverageParserOptions {

    private String LCovVersion
    private String timestamp
    private List<String> excludedPatterns

    private CoverageParserOptions() {

    }

    CoverageParserOptions(String lcovVersion, String timestamp, List<String> excPatterns) {
        this.LCovVersion = lcovVersion
        this.timestamp   = timestamp
        this.excludedPatterns = excPatterns != null ? excPatterns : []
    }

    String getLCovVersion() {
         this.LCovVersion
    }

    String getTimestamp() {
        this.timestamp
    }

    boolean isFileExcluded(File f) {
        if (this.excludedPatterns.size() > 0) {
            for (String regex : this.excludedPatterns) {
                boolean isExcluded = Pattern.matches(regex, f.absolutePath)
                if (isExcluded) {
                    return true
                }
            }
        }
        return false
    }
}
