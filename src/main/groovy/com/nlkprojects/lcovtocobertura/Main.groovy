
/**
*	Copyright (c) 2017 Kash Kabeya
*	This is free software, licensed under the Apache License, Version 2.0,
*	available in the accompanying LICENSE.txt file.
*/

package com.nlkprojects.lcovtocobertura

import com.nlkprojects.lcovtocobertura.lcov.CoverageParser
import com.nlkprojects.lcovtocobertura.lcov.CoverageParserOptions
import com.nlkprojects.lcovtocobertura.utils.Usage

class Main {

	static void main (String[] args) {

        if (validateArgs(args)) {

            Map<String, ?> parsedArgs = parseArgs(args)

            String baseDir    = new File((String)parsedArgs["-d"])
            File coverageFile = new File((String)parsedArgs["-r"])
            File outFile      = parsedArgs["-o"] != null ? new File((String)parsedArgs["-o"]) : new File("coverage.xml")
            List<String> excludePatterns = (List<String>)parsedArgs["-e"]
            CoverageParserOptions options = new CoverageParserOptions((String)parsedArgs["-V"] ,new Long(coverageFile.lastModified()).toString(), excludePatterns)
            CoverageParser parser = new CoverageParser(baseDir, options)

            XmlSlurper slurper = new XmlSlurper()
            slurper.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
            slurper.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false)

            def xml = slurper.parseText(parser.parse(coverageFile).toString())
            FileWriter writer = new FileWriter(outFile)
            xml.writeTo(writer)

        }
	}

    static boolean validateArgs(String[] args) {

        boolean baseDirSet        = false
        boolean reportFilePresent = false

        if (args.size() > 0) {
            if ((args.size() % 2) == 0) {
                for (int i = 0; i < args.size(); i+=2) {
                    if(args[i].equalsIgnoreCase("-d")) {
                        File p = args[i+1].equalsIgnoreCase(".") ? new File("") : new File(args[i+1])
                        if (p.isDirectory() && p.isAbsolute())
                        {
                            baseDirSet = true
                        } else {
                            println "ERROR: Base Directory provided is invalid: ${args[i+1]}"
                            break
                        }
                    } else if (args[i].equalsIgnoreCase("-r")) {
                        File f = new File(args[i+1])
                        if (f.isFile() && f.canRead()) {
                            reportFilePresent = true
                        } else {
                            println "ERROR: Report file provided is invalid: ${args[i+1]}"
                            break
                        }
                    }
                }
            } else {
                println "ERROR: INVALID AMOUNT OF ARGUMENTS PASSED TO THE SCRIPT.\n ARGS: ${args}\n${Usage.usage()}"
                return false
            }
        } else {
            println "ERROR: NO ARGUMENTS PASSED TO THE SCRIPT.\n${Usage.usage()}"
            return false
        }

        if(baseDirSet && reportFilePresent)
        {
            return true
        }

        println "ERROR: MISSING CRITICAL ARGUMENTS.\n${Usage.usage()}"
        false
    }

    static Map<String, ?> parseArgs (String[] args) {
        Map<String, ?> map = [:]

        for(int i = 0; i < args.size(); i+=2) {
            String s = args[i]
            if (s == "-e") {
                map[s] = args[i+1].split(",")
                continue
            }

            map[args[i]] = args[i+1]
        }

        map
    }
}
