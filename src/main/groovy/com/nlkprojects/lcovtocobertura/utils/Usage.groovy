package com.nlkprojects.lcovtocobertura.utils

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE.txt file.
 */

class Usage {
    static String usage() {
        StringBuilder sb = new StringBuilder('\n\n')
        sb.append('LCovToCobertura').append('\n').append('\n\n')
        sb.append('COMMAND').append('\n')
        sb.append('    \$java -jar ./LCovToCobertura.jar [-cdeoV]').append('\n\n')

        sb.append('DESCRIPTION').append('\n')
        sb.append('     This app converts LCov generated coverage report to XML capable of being consumed by Cobertura. Version: ').append(System.getProperty('VERSION')).append('\n\n')

        sb.append('     The Following options are available: ').append('\n')

        sb.append('     -d   The base directory of the project *REQUIRED*').append('\n')
        sb.append('     -e   Exclude files or directories pattern separated by comma').append('\n')
        sb.append('     -o   The output file where the resulting xml will be written, if none provided will output to \'./coverage.xml\'*\n')
        sb.append('     -r   Report file to be parsed *REQUIRED*\n')
        sb.append('     -V   LCOV version being used)').append('\n\n')
        return sb.toString()
    }
}
