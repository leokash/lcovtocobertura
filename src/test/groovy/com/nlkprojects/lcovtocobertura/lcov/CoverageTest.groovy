package com.nlkprojects.lcovtocobertura.lcov

import com.nlkprojects.lcovtocobertura.lcov.api.ClassCoverage
import com.nlkprojects.lcovtocobertura.lcov.api.PackageCoverage
import spock.lang.Specification


/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

class CoverageTest extends Specification {
    private static String TIMESTAMP = new Long(System.currentTimeMillis()).toString()

    def "should have a valid coverage report"() {
        setup:
        Coverage coverage = new Coverage("1.13", TIMESTAMP)
        println coverage.toString()

        expect:
        coverage != null
    }

    def "should have a valid coverage source"() {
        def coverageXML

        setup:
        Coverage coverage = new Coverage("1.13", TIMESTAMP)

        when:
        coverage.addSource("/Users/foo_fighter/projects/ios/FooBar")
        String xml = coverage.provideXML()
        println xml
        def slurper = new XmlSlurper()
        slurper.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
        slurper.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false)
        coverageXML = slurper.parseText(xml)

        then:
        coverageXML != null
        coverageXML.sources.source[0] == "/Users/foo_fighter/projects/ios/FooBar"
    }

    def "should have a valid coverage package"() {
        def coverageXML

        setup:
        Coverage coverage = new Coverage("1.13", TIMESTAMP)

        when:
        coverage.addSource("/Users/foo_fighter/projects/ios/FooBar")

        PackageCoverage pkg = new PackageCoverage("main.classes")
        pkg.addClass(new ClassCoverage("foo_h", "main/classes/main.h", 5, 3, 3, 1, 0, 0))
        pkg.addClass(new ClassCoverage("foo_m", "main/classes/main.h", 115, 93, 13, 7, 12, 5))
        coverage.addPackage(pkg)

        String xml = coverage.provideXML()
        println xml
        def slurper = new XmlSlurper()
        slurper.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
        slurper.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false)
        coverageXML = slurper.parseText(xml)

        then:
        coverageXML != null
        coverageXML.sources.source[0] == "/Users/foo_fighter/projects/ios/FooBar"
        coverageXML.packages.package[0].@'name' == "main.classes"
    }
}
