package com.nlkprojects.lcovtocobertura.lcov

import spock.lang.Specification


/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

class CoverageParserTest extends Specification {

    final String BASE_DIR = "/Users/user1/workspace/objcApp/"

    def "should have valid coverage file"() {

        String report = '''
        
        SF:/Users/user1/workspace/objcApp/main.m
        
        FN:5,-[Main main:]
        FN:20,-[Main foo]
        FN:45,-[Main bar:]
        
        FNDA:25,-[Main main:]
        FNDA:0,-[Main foo]
        FN:15,-[Main bar:]
        
        FNF:3
        FNH:2
        
        BRDA:8,2,0,0
        BRDA:8,3,1,4
        BRDA:8,3,2,7
        
        BRF:2
        BRH:1
        
        DA:5,25
        DA:6,0
        DA:7,25
        DA:8,25
        DA:9,0
        DA:20,0
        DA:45,15
        DA:47,15
        DA:52,15
        
        LF:9
        LH:6
        
        end_of_record
                                  '''
        Coverage coverage

        setup:
        String time = new Long(System.currentTimeMillis()).toString()
        CoverageParser parser = new CoverageParser(BASE_DIR, new CoverageParserOptions("1.13", time,null))

        when:
        List<String> list = report.split("\n")
        coverage = parser.parseReport(list)

        then:
        coverage != null

    }
}
