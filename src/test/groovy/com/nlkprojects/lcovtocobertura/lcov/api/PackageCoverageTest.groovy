package com.nlkprojects.lcovtocobertura.lcov.api

import spock.lang.Specification


/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

class PackageCoverageTest extends Specification {

    def "should have a valid package"() {
        setup:
        PackageCoverage pkg = new PackageCoverage("foo.baz")
        pkg.addClass(new ClassCoverage("bar_m", "foo/baz/bar.m", 4, 3, 2, 1, 0, 0))
        println pkg.toString()

        expect:
        pkg != null
        pkg.getName() == "foo.baz"
    }

    def "package should sum up classes metrics"() {
        def pkgXML

        setup:
        PackageCoverage pkg = new PackageCoverage("test.boo.wee")
        pkg.addClass(new ClassCoverage("foo_m", "test/boo/wee/foo.m", 4, 3, 2, 1, 0, 0))
        pkg.addClass(new ClassCoverage("bar_m", "test/boo/wee/bar.m", 23, 13, 12, 7, 8, 3))

        when:
        String xml = pkg.provideXML()
        println xml
        pkgXML = new XmlSlurper().parseText(xml)

        then:
        pkgXML.@'name'        == "test.boo.wee"
        pkgXML.@'complexity'  == "0"
        pkgXML.@'line-rate'   =~ /0\.592592.*/
        pkgXML.@'method-rate' =~ /0\.571428.*/
        pkgXML.@'branch-rate' == "0.375"

    }
}
