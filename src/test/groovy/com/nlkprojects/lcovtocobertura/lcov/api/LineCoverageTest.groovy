package com.nlkprojects.lcovtocobertura.lcov.api

import spock.lang.Specification

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

class LineCoverageTest extends Specification {

    def "should have valid line"() {
        setup:
        LineCoverage line = new LineCoverage(4, 0, null)
        println line.provideXML()

        expect:
        line != null
        line.identifier() == 4
    }

    def "line's info should be valid"() {

        setup:
        LineCoverage line = new LineCoverage(4, 56, null)

        expect:
        line.number            == 4
        line.hitCount          == 56
        line.branchesCount()    == 0
        line.branchesHit() == 0
    }

    def "line should have one branch"() {
        setup:
        BranchCoverage branch = BranchCoverage.createBranch(7, 0, 0, 0, {i -> null})
        LineCoverage line = new LineCoverage(7, 2, branch)
        println line.toString()

        expect:
        line.branchesCount() == 1
        line.branchesHit()   == 0
    }

    def "line should have 2 branches with one hit"() {
        setup:
        BranchCoverage branch = BranchCoverage.createBranch(7, 0, 0, 0, {i -> null})
        BranchCoverage.createBranch(7, 0, 1, 5, {i -> branch})
        LineCoverage line = new LineCoverage(7, 5, branch)
        println line.provideXML()

        expect:
        line.branchesCount() == 2
        line.branchesHit()   == 1
    }

    def "xml should match"() {

        def lineXML

        setup:
        LineCoverage line = new LineCoverage(7, 2, null)

        when:
        lineXML = new XmlSlurper().parseText(line.provideXML())

        then:
        lineXML.@branch  == false
        lineXML.@hits    == 2
        lineXML.@number  == 7
    }

    def "xml should match with conditions"() {

        def lineXML

        setup:
        BranchCoverage branch = BranchCoverage.createBranch(7, 0, 0, 0, {i -> null})
        BranchCoverage.createBranch(7, 0, 1, 11, {i -> branch})
        BranchCoverage.createBranch(7, 0, 2, 15, {i -> branch})
        LineCoverage line = new LineCoverage(67, 26, branch)

        when:
        lineXML = new XmlSlurper().parseText(line.provideXML())

        then:
        lineXML.@'condition-coverage'             == "66% (2/3)"
        lineXML.conditions.condition.size()       == 3
        lineXML.conditions.condition[0].@coverage == "0%"
        lineXML.conditions.condition[1].@coverage == "100%"

    }
}
