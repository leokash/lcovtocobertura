package com.nlkprojects.lcovtocobertura.lcov.api.collections.map

import com.nlkprojects.lcovtocobertura.lcov.api.LineCoverage
import com.nlkprojects.lcovtocobertura.lcov.api.metrics.MetricType
import spock.lang.Shared
import spock.lang.Specification

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */
class CoverageMapTest extends Specification {
    @Shared Closure<Integer> closureMock = { boolean b, MetricType t, LineCoverage l -> 0}

    def addToMap(CoverageMap<Integer, LineCoverage> map, ids) {
        ids.each {Map m ->
            m.entrySet().each { Map.Entry<Integer, Integer> e ->
                map.add(e.key, new LineCoverage(e.key, e.value, null),[MetricType.MetricTypeLineCount, MetricType.MetricTypeLineHits].asList())
            }
        }
    }

    def "single line added"() {

        setup:
        CoverageMap<Integer, LineCoverage> map = new CoverageMap<>(closureMock)

        when:
        addToMap(map, [[2:0]])

        then:
        map.size() == 1
    }

    def "multiple line added"() {

        setup:
        CoverageMap<Integer, LineCoverage> map = new CoverageMap<>(closureMock)

        when:
        addToMap(map, [[2:0], [2:4], [5:7]])

        then:
        map.size() == 2
    }

    def "addition should return false"() {

        setup:
        CoverageMap<Integer, LineCoverage> map = new CoverageMap<>(closureMock)

        when:
        addToMap(map, [[2:0]])

        then:
        !map.add(2, new LineCoverage(2, 1, null), [].asList())
    }

    def "size when empty"() {

        setup:
        CoverageMap<Integer, LineCoverage> map = new CoverageMap<>(closureMock)

        expect:
        map.isEmpty()
    }
}
