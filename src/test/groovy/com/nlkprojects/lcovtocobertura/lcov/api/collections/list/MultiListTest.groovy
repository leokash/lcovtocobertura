package com.nlkprojects.lcovtocobertura.lcov.api.collections.list

import spock.lang.Specification

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */
class MultiListTest extends Specification {

    MultiList<Integer, Integer> list

    def "addition with invalid key"() {
        setup:
        list = new MultiList<>()

        expect:
        !list.add(null, 0)
    }

    def "addition with invalid value"() {
        setup:
        list = new MultiList<>()

        expect:
        !list.add(2, null)
    }

    def "addition with invalid input"() {
        setup:
        list = new MultiList<>()

        expect:
        !list.add(null, null)
    }

    def "addition should be successful"() {
        setup:
        list = new MultiList<>()

        expect:
        list.add(1, 1)
    }

    def "addition should be successful when adding duplicate data"() {
        setup:
        list = new MultiList<>()
        list.add(1,1)

        expect:
        list.add(1, 1)
        list.size() == 2
    }

    def "removal of object by key with invalid ID"() {
        setup:
        list = new MultiList<>()
        list.add(1,1)

        expect:
        !list.remove(2)
        list.size() == 1
    }

    def "removal of object with null key"() {
        setup:
        list = new MultiList<>()
        list.add(1,1)

        expect:
        !list.remove(null)
        list.size() == 1
    }

    def "removal of object by key with valid key"() {

        setup:
        list = new MultiList<>()
        list.add(1, 1)

        expect:
        list.size() == 1
        list.remove(1)
        list.size() == 0
    }

    def "removal via key mechanism 1"() {
        setup:
        MultiList<Integer, String> list = new MultiList<>()

        when:
        list.add(1, "X")
        list.add(1, "Y")
        list.add(2, "Z")

        then:
        list.size() == 3
        list.remove(1)
        list.size() == 1
    }

    def "removal of null object"() {
        setup:
        list = new MultiList<>()

        expect:
        !list.removeElement(null)
    }

    def "removal of object by value"() {

        setup:
        list = new MultiList<>()
        list.add(1, 1)
        list.add(1, 2)
        list.add(2, 1)

        expect:
        list.size() == 3
        list.removeElement(1)
        list.size() == 1
    }

    def "removal of all objects"() {

        setup:
        list = new MultiList<>()
        list.add(1, 1)
        list.add(1, 2)
        list.add(2, 1)

        when:
        list.clear()

        then:
        list.size() == 0
    }
}
