package com.nlkprojects.lcovtocobertura.lcov.api

import com.nlkprojects.lcovtocobertura.lcov.api.metrics.MetricType
import spock.lang.Specification

/**
 * Created by leokash on 09/01/17.
 */
class MethodCoverageTest extends Specification {

    def "should have a valid method coverage instance"() {
        setup:
        MethodCoverage mc = new MethodCoverage("-[Foo bar]", 34)
        println mc.toString()

        expect:
        mc != null
        mc.identifier() == "-[Foo bar]"
        mc.startLine()  == 34
    }

    def "adding an already present line shouldn't change the metrics"() {
        setup:
        MethodCoverage mc = new MethodCoverage("-[Foo bar]", 34)
        mc.addLine(new LineCoverage(35, 5, null))
        mc.addLine(new LineCoverage(35, 6, null))

        expect:
        mc != null
        mc.identifier() == "-[Foo bar]"
        mc.startLine()  == 34
        mc.getCount(MetricType.MetricTypeLineCount) == 1
    }

    def "should have a valid method coverage with signature"() {
        def method

        setup:
        MethodCoverage mc = new MethodCoverage("-[Foo bar:withBaz:]", "()V", 134)

        when:
        String xml = mc.provideXML()
        println xml
        method = new XmlSlurper().parseText(xml)

        then:
        method != null
        method.@signature     == "()V"
        method.@'name'        == "-[Foo bar:withBaz:]"
        method.@'line-rate'   == "0.0"
        method.@'branch-rate' == "0.0"
    }

    def "should have a valid method coverage with lines"() {
        def method

        setup:
        MethodCoverage mc = new MethodCoverage("-[Foo bar:withBaz:]", "()V", 134)
        mc.addLine(new LineCoverage(134, 56, null))
        mc.addLine(new LineCoverage(139, 13, null))
        mc.addLine(new LineCoverage(142, 0, null))

        when:
        String xml = mc.provideXML()
        println xml
        method = new XmlSlurper().parseText(xml)

        then:
        method.lines.line.size() == 3
        method.@'line-rate'      =~ /0\.666.*/
        method.@'branch-rate'    == "0.0"
    }

    def "should have a valid method coverage with lines and branches"() {
        def method

        setup:
        MethodCoverage mc = new MethodCoverage("-[Foo bar:withBaz:]", null, 134)
        mc.addLine(new LineCoverage(134, 56, null))

        BranchCoverage branch = BranchCoverage.createBranch(136, 0, 0, 0, {i -> null})
        BranchCoverage.createBranch(136, 0, 0, 1, {i -> branch})
        mc.addLine(new LineCoverage(136, 1, branch))

        mc.addLine(new LineCoverage(139, 13, null))

        branch = BranchCoverage.createBranch(141, 0, 0, 4, {i -> null})
        BranchCoverage.createBranch(141, 0, 1, 0, {i -> branch})
        BranchCoverage.createBranch(141, 1, 2, 6, {i -> branch})
        BranchCoverage.createBranch(141, 1, 3, 20, {i -> branch})
        BranchCoverage.createBranch(141, 2, 4, 0, {i -> branch})
        mc.addLine(new LineCoverage(141, 30, branch))

        mc.addLine(new LineCoverage(142, 0, null))

        when:
        String xml = mc.provideXML()
        println xml
        method = new XmlSlurper().parseText(xml)

        then:
        method.lines.line.size() == 5
        method.@'line-rate'      == "0.8"
        method.@'branch-rate'    =~ /0\.571428.*/
    }
}
