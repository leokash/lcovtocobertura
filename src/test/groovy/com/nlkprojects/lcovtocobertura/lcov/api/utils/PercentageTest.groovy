package com.nlkprojects.lcovtocobertura.lcov.api.utils

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll


/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

class PercentageTest extends Specification {

    @Shared TestObject object = new TestObject()

    @Unroll
    "testing output"() {
        expect:
        a == b

        where:
        a                         || b

        object.percent(1, 2, 0)   || "50"
        object.percent(1, 3, 2)   || "33.33"
        object.percent(1, 4, 2)   || "25"
        object.percent(1, 700, 2) || "0.14"
        object.percent(5, 5, 2)   || "100"
        object.percent(0, 6, 3)   || "0"
    }

    class TestObject implements Percentage {

    }
}
