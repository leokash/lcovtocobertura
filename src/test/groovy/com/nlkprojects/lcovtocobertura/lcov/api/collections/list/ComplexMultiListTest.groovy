package com.nlkprojects.lcovtocobertura.lcov.api.collections.list

import com.nlkprojects.lcovtocobertura.lcov.api.utils.Identifiable
import spock.lang.Specification

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */
class ComplexMultiListTest extends Specification {

    ComplexMultiList<Integer, Item> multiList

    //Addition Test
    def "multiList should created"() {
        expect:
        new ComplexMultiList<>() != null
    }

    def "adding null object"() {
        setup:
        multiList = new ComplexMultiList<>()

        expect:
        !multiList.add(null)
        multiList.size() == 0
    }

    def "adding valid object"() {
        setup:
        multiList = new ComplexMultiList<>()

        when:
        multiList.add(new Item(1, "Item 1"))

        then:
        multiList.size() == 1
    }

    def "adding multiple valid objects"() {
        setup:
        multiList = new ComplexMultiList<>()

        when:
        multiList.add(new Item(1, "Item 1"))
        multiList.add(new Item(2, "Item 1"))

        then:
        multiList.size() == 2
    }

    def "adding duplicate objects"() {
        setup:
        multiList = new ComplexMultiList<>()

        when:
        multiList.add(new Item(1, "Item 1"))
        multiList.add(new Item(1, "Item 1"))

        then:
        multiList.size() == 2
    }

    //Removal Test
    def "removal of null object"() {

        setup:
        multiList = new ComplexMultiList<>()

        expect:
        !multiList.removeElement(null)
    }

    def "removal of invalid object"() {

        setup:
        multiList = new ComplexMultiList<>()

        expect:
        !multiList.removeElement(new Item(1, "C"))
    }

    def "removal of object"() {

        Item item

        setup:
        multiList = new ComplexMultiList<>()
        item = new Item(1, "Item 1")
        multiList.add(item)

        expect:
        multiList.removeElement(item)
        multiList.size() == 0
    }

    def "removal of object by value with multiple objects"() {

        Item item

        setup:
        multiList = new ComplexMultiList<>()
        multiList.add((item = new Item(1, "Item 1")))
        multiList.add(new Item(1, "Item 2"))

        expect:
        multiList.size() == 2
        multiList.removeElement(item)
        multiList.size() == 1
    }

    //All objects retrieval
    def "all objects when empty"() {
        setup:
        multiList = new ComplexMultiList<>()

        expect:
        multiList.allObjects().isEmpty()
    }

    def "all objects"() {
        setup:
        multiList = new ComplexMultiList<>()

        when:
        multiList.add(new Item(1, "X"))
        multiList.add(new Item(1, "Y"))
        multiList.add(new Item(2, "Z"))

        then:
        multiList.allObjects().size() == 3
    }

    def "all object with filter wielding nothing"() {
        setup:
        multiList = new ComplexMultiList<>()

        when:
        multiList.add(new Item(2, "Z"))

        then:
        multiList.allObjects({Item i -> i.identifier() == 1}).isEmpty()
        multiList.allObjects({Item i -> i.getName() == "Y"}).isEmpty()
    }

    def "all object with filter wields result"() {
        setup:
        multiList = new ComplexMultiList<>()

        when:
        multiList.add(new Item(2, "Z"))
        multiList.add(new Item(1, "Z"))

        then:
        multiList.allObjects({Item i -> i.identifier() == 1}).size() == 1
        multiList.allObjects({Item i -> i.getName() == "Z"}).size()  == 2
    }

    class Item implements Identifiable<Integer> {

        private int    id
        private String name

        Item (int i, String n) {
            this.id   = i
            this.name = n
        }

        String getName() {
            this.name
        }

        @Override
        Integer identifier() {
            this.id
        }
    }
}
