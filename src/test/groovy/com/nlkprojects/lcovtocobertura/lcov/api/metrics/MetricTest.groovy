package com.nlkprojects.lcovtocobertura.lcov.api.metrics

import spock.lang.Shared
import spock.lang.Specification

/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

class MetricTest extends Specification {

    @Shared MetricType type = MetricType.MetricTypeLineCount

    def "increment"() {
        setup:
        Metric m = new Object() as Metric

        when:
        m.increment(type)

        then:
        m.getCount(type) == 1
    }

    def "increment with value"() {
        setup:
        Metric m = new Object() as Metric

        when:
        m.increment(type, 5)

        then:
        m.getCount(type) == 5
    }

    def "count when not incremented"() {
        setup:
        Metric m = new Object() as Metric

        expect:
        m.getCount(type) == 0
    }

    def "count with invalid metric"() {
        setup:
        Metric m = new Object() as Metric

        when:
        m.increment(type)

        then:
        m.getCount(MetricType.MetricTypeBranchHits) == 0
    }
}
