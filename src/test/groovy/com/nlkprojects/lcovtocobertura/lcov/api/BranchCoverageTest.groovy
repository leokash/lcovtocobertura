package com.nlkprojects.lcovtocobertura.lcov.api

import spock.lang.Specification
import groovy.xml.*
import spock.lang.Unroll


/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

class BranchCoverageTest extends Specification {

    @Unroll
    "expect valid branch"() {
        setup:
        BranchCoverage branch = BranchCoverage.createBranch(1,2, 0, 0, {i -> null})
        BranchCoverage.createBranch(1, 2, 1, 0, {i -> branch})
        println branch.toString()

        expect:
        branch              != null
        branch.identifier   == 1
        branch.pathsCount() == 2
        branch.pathsHit()   == 0
    }

    @Unroll
    "expect coverage of 1"() {
        setup:
        BranchCoverage branch = BranchCoverage.createBranch(1,2, 0, 0, {i -> null})
        BranchCoverage.createBranch(1, 2, 1, 1, {i -> branch})

        expect:
        branch              != null
        branch.identifier   == 1
        branch.pathsCount() == 2
        branch.pathsHit()   == 1
    }

    def "expect not equal"() {
        setup:
        BranchCoverage b1 = BranchCoverage.createBranch(1, 2, 0, 0, {i -> null})
        BranchCoverage b2 = BranchCoverage.createBranch(1, 3, 0, 0, {i-> null})

        expect:
        b1 != b2
    }

    @Unroll
    "expect xml match"() {

        def conditions //Branch ranch

        setup:
        BranchCoverage branch = BranchCoverage.createBranch(1,2, 0, 0, {i -> null})

        when:
        String xml = branch.provideXML()
        conditions = new XmlSlurper().parseText(xml)

        then:
        conditions != null
        conditions.condition.size() == 1

    }

    @Unroll
    "expect xml match 1"() {

        def conditions //Branch

        setup:
        BranchCoverage branch = BranchCoverage.createBranch(1,2, 1, 1, {i -> null})
        BranchCoverage.createBranch(1,2, 0, 0, {i -> branch})

        when:
        conditions = new XmlSlurper().parseText(branch.provideXML())

        then:
        conditions.condition.size()       == 2
        conditions.condition[0].@coverage == "0%"
        conditions.condition[1].@coverage == "100%"
        conditions.condition[1].@type     == "jump"
    }
}
