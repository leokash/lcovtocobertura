package com.nlkprojects.lcovtocobertura.lcov.api

import spock.lang.Specification


/**
 * 	Copyright (c) 2017 Kash Kabeya
 * 	This is free software, licensed under the Apache License, Version 2.0,
 * 	available in the accompanying LICENSE file.
 */

class ClassCoverageTest extends Specification {

    def "should have valid class"() {
        setup:
        ClassCoverage clazz = new ClassCoverage("main_m", "foo/main.m", 9, 3, 2, 1, 3, 2)
        println clazz.toString()

        expect:
        clazz != null
        clazz.name     == "main_m"
        clazz.filename == "foo/main.m"
    }

    def "should have valid xml with methods"() {
        def clazzXML

        setup:
        ClassCoverage clazz = new ClassCoverage("bar_m", "bar/baz/bar.m", 45, 12, 5, 4, 12, 10)
        clazz.addMethod(new MethodCoverage("-[Bar bar]", 6))
        clazz.addMethod(new MethodCoverage("-[Bar baz]", 30))
        clazz.addMethod(new MethodCoverage("-[Bar foo]", 35))
        clazz.addMethod(new MethodCoverage("-[Bar fuzz]", 50))
        clazz.addMethod(new MethodCoverage("-[Bar fizz]", 68))

        when:
        String xml = clazz.provideXML()
        println xml
        clazzXML = new XmlSlurper().parseText(xml)

        then:
        clazzXML.@'name'                   == "bar_m"
        clazzXML.@'filename'               == "bar/baz/bar.m"
        clazzXML.@'method-rate'            == "0.8"
        clazzXML.@'line-rate'              =~ /0\.26666.*/
        clazzXML.@'branch-rate'            =~ /0\.83333.*/
        clazzXML.methods.method.size()     == 5
        clazzXML.methods.method[0].@'name' == "-[Bar bar]"
    }
}
